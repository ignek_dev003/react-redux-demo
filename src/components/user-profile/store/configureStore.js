import { createStore, applyMiddleware, compose } from 'redux'
import storage from 'redux-persist/es/storage';
// import { createLogger } from 'redux-logger';
import userProfileReducer from './userProfileReducer';
import { reducer as formReducer } from 'redux-form';
import { persistStore, persistCombineReducers } from 'redux-persist';

const persistConfig = {
  key : 'root',
  storage : storage
}
const middlewares = [];
if(process.env.NODE_ENV === 'development'){
  // middlewares.push(createLogger());
}

const rootReducer = {
  ...userProfileReducer,
  form:formReducer
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const persistCombineReducer = persistCombineReducers(persistConfig, rootReducer);

export const store = createStore(
  persistCombineReducer,
  undefined,
  composeEnhancers(applyMiddleware(...middlewares)),
);

export const persistor = persistStore(store)
