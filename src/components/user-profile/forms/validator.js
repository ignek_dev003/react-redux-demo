const required = value =>
  value || typeof value === 'number'
    ? undefined
    : 'This field is required'

const minLength = min => value =>
  value && value.length < min
    ? `Must be ${min} characters or more`
    : undefined

const maxLength = max => value =>
  value && value.length > max
    ? `Must be ${max} characters or less`
    : undefined

const matchLength = length => value =>
    value && value.length !== length
    ? `Must be ${length} characters`
    : undefined

const alphaNumeric = value =>
  value && /[^a-zA-Z0-9 ]/i.test(value)
    ? 'Only alphanumeric characters are allowed'
    : undefined

const numeric = value =>
  value && isNaN(Number(value))
    ? 'Only numeric characters are allowed'
    : undefined

const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined

const phoneNumber = value =>
  value && !/^(0|[1-9][0-9]{9})$/i.test(value)
    ? 'Invalid phone number, must be 10 digits'
    : undefined

export { required, minLength, maxLength, matchLength, alphaNumeric, numeric, email, phoneNumber }