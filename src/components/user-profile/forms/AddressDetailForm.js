import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Redirect } from 'react-router-dom'
import { getAdjoinFormsLink, resetUserProfileForms } from '../menuItemsConfig';
import * as validator from './validator';

class AddressDetailForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      redirectNext : false,
      adjoinFormsLink : getAdjoinFormsLink(this.constructor.name)
    }
  }

  handleSubmit = (value) => {
    console.log("Below is submitted form value for AddressDetailForm");
    console.log(value);
    if(this.state.adjoinFormsLink && this.state.adjoinFormsLink.next){
      this.setState({redirectNext:true});
    }
    else{
      resetUserProfileForms(this.props.dispatch);
    }
  }

  renderField = ({
    input,
    label,
    type,
    meta: { touched, error, warning }}) => (
    <div className="form-group">
      <label htmlFor={input.name}>{label}</label>
      <input {...input} id={input.name} type={type} className="form-control" />
      {
        touched
        && ((error && <small className="form-text text-danger">{error}</small>)
        || (warning && <small className="form-text text-secondary">{warning}</small>))
      }
    </div>
  );

  render(){
    const { handleSubmit, pristine, reset, invalid, submitting } = this.props;
    const { redirectNext, adjoinFormsLink } = this.state;
    let template = null;
    if(redirectNext){
      template = <Redirect to={adjoinFormsLink.next} />
    }
    else{
      template = (
        <form autoComplete="off" onSubmit={handleSubmit(this.handleSubmit)}>
          <div className="form-group">
            <label htmlFor="address">Address</label>
            <Field
              id="address"
              name="address"
              component="textarea"
              className="form-control" />
          </div>
          <Field
            type="text"
            name="city"
            label="City"
            component={this.renderField}
            validate={[validator.required, validator.alphaNumeric, validator.minLength(2)]} />
          <Field
            type="text"
            name="state"
            label="State"
            component={this.renderField}
            validate={[validator.required, validator.alphaNumeric, validator.minLength(2)]} />
          <Field
            type="text"
            name="zipcode"
            label="ZIP code"
            component={this.renderField}
            validate={[validator.required, validator.alphaNumeric, validator.minLength(2)]} />
          <div className="d-flex">
            {
              adjoinFormsLink && adjoinFormsLink.prev &&
              <a href={adjoinFormsLink.prev} className="btn btn-outline-primary mr-auto"> Previous </a>
            }
            <button type="submit" className="btn btn-primary mr-3" disabled={pristine || invalid || submitting}>Next</button>
            <button type="reset" className="btn btn-outline-danger" disabled={submitting} onClick={reset}>Clear</button>
          </div>
        </form>
      );
    }
    return template;
  }
};

export default reduxForm({
  destroyOnUnmount: false,
  form: 'AddressDetailForm',
})(AddressDetailForm);
