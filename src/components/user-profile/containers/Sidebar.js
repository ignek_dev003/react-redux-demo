import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';

class Sidebar extends Component {
  render() {
    const { menuItemsConfig } = this.props;
    const template = (
      <div className="list-group">
        {
          (menuItemsConfig && menuItemsConfig.menuItems
            ? menuItemsConfig.menuItems.map((menuItem, key) =>
              <NavLink
                key = {key}
                to = {menuItem.menuLink}
                className = {menuItemsConfig.menuItemClassName.join(" ")}
                activeClassName = {menuItemsConfig.activeMenuItemClassName}>
                  {menuItem.menuTitle}
              </NavLink>)
            : <p className="m-0">Unable to load configuration for menu items</p>)
        }
      </div>
    );
    return template;
  }
}

export default Sidebar;
