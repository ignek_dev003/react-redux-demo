import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { menuItemsConfig } from '../menuItemsConfig';
import Header from './Header';
import Sidebar from './Sidebar';
import Content from './Content';

class RootContainer extends Component {
  render() {
    const template = (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <Header />
          </div>
        </div>
        <div className="row">
          <BrowserRouter>
            <div className="col-md-3">
              <Sidebar menuItemsConfig={menuItemsConfig} />
            </div>
            <div className="col-md-9">
              <Content menuItemsConfig={menuItemsConfig} />
            </div>
          </BrowserRouter>
        </div>
      </div>
    );
    return template;
  }
}

export default RootContainer;
