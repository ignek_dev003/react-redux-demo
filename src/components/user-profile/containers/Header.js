import React, { Component } from 'react';

class Header extends Component {
  render() {
    const template = (
      <div className="bg-light rounded-lg text-center py-4 my-4">
        <h2 className="mb-0">User Profile Detail</h2>
        <p className="mb-0">React + Redux demo with persistent state</p>
      </div>
    );
    return template;
  }
}

export default Header;
