import React, { Component } from 'react';
import { Route } from 'react-router-dom';

class Content extends Component {
  render() {
    const { menuItemsConfig } = this.props;
    const defaultMenuItem = (menuItemsConfig.menuItems)?menuItemsConfig.menuItems[0]:undefined;
    const template = (
      <div className="col-md-12 bg-light rounded-lg py-3">
        <div className="default-route">
          {
            (defaultMenuItem && <Route exact path='/' component={defaultMenuItem.component} />)
          }
        </div>
        {
          menuItemsConfig && menuItemsConfig.menuItems
          ? menuItemsConfig.menuItems.map((menuItem, key) =>
              <Route
                key={key}
                path={menuItem.menuLink}
                component={menuItem.component} />
            )
          : <p className="m-0">Unable to load configuration for menu items</p>
        }
      </div>
    );
    return template;
  }
}

export default Content;
