import React, { Component } from 'react';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { persistor, store } from './store/configureStore';
import { Provider } from 'react-redux';
import RootContainer from './containers/RootContainer';

class UserProfile extends Component {
  render() {
    const template = (
      <div className="user-profile-wrapper">
        <PersistGate persistor={persistor}>
          <Provider store={store}>
            <RootContainer/>
          </Provider>
        </PersistGate>
      </div>
    );
    return template;
  }
}

export default UserProfile;
